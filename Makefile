#  fotoxx Makefile
#
#  export CXX=g++          gnu compiler
#  export CXX=clang++      clang compiler
#  export DEBUG=x          debug build with address checking

ifeq ($(CXX), clang++)
   ifdef DEBUG
      CXXFLAGS += -Wall -g -O0 -fsanitize=address
      LDFLAGS += -fsanitize=address
   else
      CXXFLAGS += -Wall -g -O2
   endif
else
   ifdef DEBUG
      CXXFLAGS += -Wall -g -rdynamic -O0 -fsanitize=address -Wno-format-truncation
      LDFLAGS += -fsanitize=address
   else
      CXXFLAGS += -Wall -g -rdynamic -O2 -Wno-format-truncation
   endif
endif

PREFIX ?= /usr
PKG_CONFIG ?= pkg-config

# target install folders
BINDIR = $(PREFIX)/bin
SHAREDIR = $(PREFIX)/share/fotoxx
DATADIR = $(SHAREDIR)/data
ICONDIR = $(SHAREDIR)/icons
IMAGEDIR = $(SHAREDIR)/images
LOCALESDIR = $(SHAREDIR)/locales
DOCDIR = $(PREFIX)/share/doc/fotoxx
MANDIR = $(PREFIX)/share/man/man1
APPDATADIR = $(PREFIX)/share/appdata
MENUFILE = $(PREFIX)/share/applications/fotoxx.desktop

CFLAGS = $(CXXFLAGS) $(CPPFLAGS) -c                \
   `$(PKG_CONFIG) --cflags gtk+-3.0`               \
   -I/usr/include/clutter-1.0/                     \
   -I/usr/include/cogl/                            \
   -I/usr/include/json-glib-1.0/                   \
   -I/usr/include/clutter-gtk-1.0/                 \
   -I/usr/include/champlain-gtk-0.12/              \
   -I/usr/include/champlain-0.12/                  \
   -I/usr/include/libchamplain-gtk-0.12/           \
   -I/usr/include/libchamplain-0.12/

LIBS = `$(PKG_CONFIG) --libs gtk+-3.0`                                              \
         -lrt -lpthread -llcms2 -ltiff -lpng -ljpeg                                 \
         -lclutter-1.0 -lclutter-gtk-1.0 -lchamplain-0.12 -lchamplain-gtk-0.12

ALLFILES = fotoxx.o f.widgets.o f.file.o f.gallery.o f.albums.o f.area.o f.meta.o   \
           f.edit.o f.enhance.o f.effects.o f.warp.o f.combine.o f.mashup.o          \
           f.tools.o f.process.o f.pixmap.o zfuncs.o

fotoxx: $(ALLFILES)
	$(CXX) $(LDFLAGS) $(ALLFILES) $(LIBS) -o fotoxx \

fotoxx.o: fotoxx.cc fotoxx.h
	$(CXX) fotoxx.cc $(CFLAGS) -o fotoxx.o \

f.widgets.o: f.widgets.cc  fotoxx.h
	$(CXX) f.widgets.cc $(CFLAGS) \

f.file.o: f.file.cc  fotoxx.h
	$(CXX) f.file.cc $(CFLAGS) \

f.gallery.o: f.gallery.cc  fotoxx.h
	$(CXX) f.gallery.cc $(CFLAGS) \

f.albums.o: f.albums.cc  fotoxx.h
	$(CXX) f.albums.cc $(CFLAGS) \

f.area.o: f.area.cc  fotoxx.h
	$(CXX) f.area.cc $(CFLAGS) \

f.meta.o: f.meta.cc  fotoxx.h
	$(CXX) f.meta.cc $(CFLAGS) \

f.edit.o: f.edit.cc  fotoxx.h
	$(CXX) f.edit.cc $(CFLAGS) \

f.enhance.o: f.enhance.cc  fotoxx.h
	$(CXX) f.enhance.cc $(CFLAGS) \

f.effects.o: f.effects.cc  fotoxx.h
	$(CXX) f.effects.cc $(CFLAGS) \

f.warp.o: f.warp.cc  fotoxx.h
	$(CXX) f.warp.cc $(CFLAGS) \

f.combine.o: f.combine.cc  fotoxx.h
	$(CXX) f.combine.cc $(CFLAGS) \

f.mashup.o: f.mashup.cc  fotoxx.h
	$(CXX) f.mashup.cc $(CFLAGS) \

f.tools.o: f.tools.cc  fotoxx.h
	$(CXX) f.tools.cc $(CFLAGS) \

f.process.o: f.process.cc  fotoxx.h
	$(CXX) f.process.cc $(CFLAGS) \

f.pixmap.o: f.pixmap.cc  fotoxx.h
	$(CXX) f.pixmap.cc $(CFLAGS) \

zfuncs.o: zfuncs.cc  zfuncs.h
	$(CXX) zfuncs.cc $(CFLAGS)                   \
	### -D DOCDIR=\"$(DOCDIR)\"      SUSE only   \

install: fotoxx uninstall
	mkdir -p  $(DESTDIR)$(BINDIR)
	mkdir -p  $(DESTDIR)$(DATADIR)
	mkdir -p  $(DESTDIR)$(ICONDIR)
	mkdir -p  $(DESTDIR)$(IMAGEDIR)
	mkdir -p  $(DESTDIR)$(LOCALESDIR)
	mkdir -p  $(DESTDIR)$(DOCDIR)
	mkdir -p  $(DESTDIR)$(MANDIR)
	mkdir -p  $(DESTDIR)$(PREFIX)/share/applications
	mkdir -p  $(DESTDIR)$(APPDATADIR)
	cp -f     fotoxx $(DESTDIR)$(BINDIR)
	cp -f -R  data/* $(DESTDIR)$(DATADIR)
	cp -f -R  images/* $(DESTDIR)$(IMAGEDIR)
	cp -f -R  locales/* $(DESTDIR)$(LOCALESDIR)
	cp -f -R  doc/* $(DESTDIR)$(DOCDIR)
	gzip -f -9 $(DESTDIR)$(DOCDIR)/changelog
	cp -f -R  appdata/* $(DESTDIR)$(APPDATADIR)
	# man page
	cp -f doc/fotoxx.man fotoxx.1
	gzip -f -9 fotoxx.1
	cp fotoxx.1.gz $(DESTDIR)$(MANDIR)
	rm -f fotoxx.1.gz
	# menu (desktop) file
	cp -f fotoxx.desktop $(DESTDIR)$(MENUFILE)
	cp -f fotoxx.png $(DESTDIR)$(ICONDIR)

uninstall:
	rm -f  $(DESTDIR)$(BINDIR)/fotoxx
	rm -f -R  $(DESTDIR)$(LOCALESDIR)
	rm -f -R  $(DESTDIR)$(SHAREDIR)
	rm -f -R  $(DESTDIR)$(DOCDIR)
	rm -f  $(DESTDIR)$(MANDIR)/fotoxx.1.gz
	rm -f  $(DESTDIR)$(MENUFILE)

clean:
	rm -f  fotoxx
	rm -f  *.o

