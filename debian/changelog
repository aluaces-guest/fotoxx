fotoxx (20.08-1) unstable; urgency=medium

  * New upstream version 20.08
  * debian/copyright:
    - Update Copyright Years and contact email
    - Add https to Upstream contact webpage
    - Add license CC0 to appdata/fotoxx.metadata.xml
  * debian/patches:
    - Removes patch 0001-Add-primary-category-to-desktop-file.patch.
      (Applied by upstream)
    - Adds patches for switching off 'phone home' by default.
      Removes old 0002-Do-not-phone-home.patch.
    - Adds 0002-manage-has-bad-whatis-entry.patch
      Fix manpage line breaks and 'what is' section.
    - Adds/Edit Patches headers.
    - Updates series file.
  * debian/control:
    - Bumps Standars Version to 4.5.0.
    - Adds Rules-Requires-Root: no.
    - Updates package Description.
    - Updates suggests packages.
    - Adds https to Homepage field.
  * debian/rules:
    - Removes override_dh_link and override_dh_fixperms-arch.
      Not needed anymore.
    - Removes extra license file.

 -- Santiago Torres Batan <santi73@gmail.com>  Mon, 24 Feb 2020 12:45:54 -0300

fotoxx (19.17-1) unstable; urgency=medium

  [ Santiago Torres Batan ]
  * debian/watch: Fix watch URL pattern
  * New upstream version 19.17
  * debian/compat: No needed it anymore as switch to dh 12
  * debian/copyright: Update copyright years
  * debian/control:
    - Update debhelper version to 12
    - Bumps Standars-Version 4.4.1
  * debian/patches: Remove applied by upstream patches:
    - Fix-cross-compilation.patch
    - Fix-ftbfs-in-freebsd.patch
    - fix-FTBFS-using-clang-instead-of-gcc.patch
  * debian/patches: Add patches and update series file
    - debian/patches/0001-Add-primary-category-to-desktop-file.patch
    - debian/patches/0002-Do-not-phone-home.patch
  * Remove debian/fotoxx-common.doc-base:
    - Not suitable documentation file, avoid installing it.
  * Finalize debian/changelog

  [ Maximiliano Curia ]
  * watch file: use https uris
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Mon, 14 Oct 2019 13:44:36 +0200

fotoxx (18.07.2-1) unstable; urgency=medium

  [ Santiago Torres Batan ]
  * New upstream version 18.07.2 (closes: #916010)
  * Refresh Patches
  * d/control: Update Build-Depends, Recommends and Suggests
  * d/rules: Add +bindnow flag to DEB_BUILD_MAINT_OPTIONS

  [ Maximiliano Curia ]
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Wed, 23 Jan 2019 12:58:16 -0300

fotoxx (18.07-1) sid; urgency=medium

  [ Maximiliano Curia ]
  * Update Vcs fields

  [ Santiago Torres Batan ]
  * Bumps Standards-Version
  * New upstream version 18.07
  * Updated Copyright Years.
  * Refreshed Patches
  * debian/rules: Remove trailing spaces
  * Fixed HTML documentation filepaths in debian/rules
    and fotoxx-common.doc-base

 -- Santiago Torres Batan <santi73@gmail.com>  Mon, 13 Aug 2018 15:42:18 -0300

fotoxx (18.01.1-2) sid; urgency=medium

  * New revision
  * Call the chmod only on arch builds (Closes: 886704)
    Thanks to Norbert Preining for the report.
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Tue, 16 Jan 2018 11:04:51 -0300

fotoxx (18.01.1-1) sid; urgency=medium

  [ Santiago Torres Batan ]
  * debian/copyright: update years.
  * debian/control: Bump to Standards-Version 4.1.1. No changes.
  * debian/patches
    - Added Fix-cross-compilation.patch.
    Thanks to Helmut Grohne. (closes: #866524)
  * debian/fotoxx.install
    - Do not install fotoxx-snap, /usr/bin/fotoxx duplicate
      Fix lintian warning
  * Fix Appstream metadata path and permissions (lintians warning)
    - debian/rules: override dh_fixperms
    - fotoxx.install: fix path

  [ Maximiliano Curia ]
  * New upstream release (18.01.1).
  * Bump Standards-Version to 4.1.3.
  * Refresh patches
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Thu, 04 Jan 2018 16:00:14 -0300

fotoxx (17.08.1-1) sid; urgency=medium

  * New upstream release (17.08.1).
  * Bump to Standards-Version 4.0.1, change priority to optional
  * Add a .gitattributes file to use dpkg-mergechangelogs
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Mon, 07 Aug 2017 00:10:16 +0200

fotoxx (16.11.1-1) unstable; urgency=medium

  * New upstream release (16.11.1).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 13 Dec 2016 15:03:38 +0100

fotoxx (16.11-1) unstable; urgency=medium

  * New upstream release (16.11).
  * Refresh patches

 -- Maximiliano Curia <maxy@debian.org>  Thu, 10 Nov 2016 16:32:46 +0100

fotoxx (16.10.3-1) unstable; urgency=medium

  * New upstream release (16.10.3).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 31 Oct 2016 13:30:12 +0100

fotoxx (16.10.1-1) unstable; urgency=medium

  * New upstream release (16.10.1).

 -- Maximiliano Curia <maxy@debian.org>  Sun, 09 Oct 2016 10:40:08 +0200

fotoxx (16.08.1-1) unstable; urgency=medium

  * New upstream release (16.08.1).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 02 Sep 2016 18:10:34 +0200

fotoxx (16.08-1) unstable; urgency=medium

  * New upstream release (16.08).
  * Bump Standards-Version to 3.9.8, no changes needed.
  * Add libraw-dev build dependency.

 -- Maximiliano Curia <maxy@debian.org>  Mon, 08 Aug 2016 23:42:13 +0200

fotoxx (16.05.1-1) unstable; urgency=medium

  [ Santiago Torres Batan  ]
  * New Upstream release.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 12 May 2016 18:46:41 +0200

fotoxx (16.03.1-1) unstable; urgency=medium

  [ Santiago Torres Batan  ]
  * New Upstream release.

  [ Maximiliano Curia ]
  * New upstream release (16.03.1).
  * Add new patch: 0002-fix-FTBFS-using-clang-instead-of-gcc.patch.
    Thanks to Arthur Marble for the report and the patch (Closes: #812720)
  * Bump Standards-Version to 3.9.7, no changes needed.
  * debian/control: Update Vcs-Browser and Vcs-Git fields

 -- Maximiliano Curia <maxy@debian.org>  Mon, 28 Mar 2016 11:01:19 +0200

fotoxx (15.11.1-3) unstable; urgency=medium

  * Improve the dh_link rule so it doesn't end with the same file in both
    packages. (Closes: #804701)

 -- Maximiliano Curia <maxy@debian.org>  Tue, 10 Nov 2015 19:49:42 +0100

fotoxx (15.11.1-2) unstable; urgency=medium

  * Add patch: 0001-Fix-ftbfs-in-freebsd.patch

 -- Maximiliano Curia <maxy@debian.org>  Tue, 10 Nov 2015 13:27:03 +0100

fotoxx (15.11.1-1) unstable; urgency=low

  [ Maximiliano Curia ]
  * New upstream release (15.11.1).
  * Drop patches applied upstream.

  [ Santiago Torres Batan ]
  * debian/copyright
    - Updated years and Source URL.

  * debian/control
    - Added hugin to Suggests packages.

  * debian/menu
    - Delete menu file as .Desktop is provided by package.

 -- Santiago Torres Batan <santi73@gmail.com>  Tue, 10 Nov 2015 03:48:21 -0300

fotoxx (14.10.2-1) unstable; urgency=medium

  [ Santiago Torres Batan ]
  * New upstream release
  * wrap-and-sort

  * debian/control
    - Added Vcs-{Browser,Git}
    - Bump Standards-Version to 3.9.6

  * debian/patches
    - Deleted makefile_changes.patch, privacy-breach-generic_fix.patch
      (were partially applied)
    - 0001-Do-not-track-users.patch: removes the code that counts users.
    - 0002-Do-not-check-for-new-releases.patch: removes the code that
      tries to download new releases. (closes: #761828)
    - 0003-Remove-Geo-download-function: removes insecured download function

  * debian/rules
    - Removed USE_XDG flag. No more needed because of upstream changes in
      Makefile.

  * debian/copyright
    - Upgraded to format 1.0

  [ Maximiliano Curia ]
  * Update copyright license to GPL-3+.
  * Add myself to the uploaders list.
  * New upstream release (14.10.2).
  * Refresh patches.

 -- Maximiliano Curia <maxy@debian.org>  Wed, 22 Oct 2014 22:20:36 +0200

fotoxx (14.07.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control
    - Moved libexiftool-perl from Recommends to Depends.
    - Replaced xgd-open with xdg-utils (closes: #743330),
      and moved to Depends.
    - Added dcraw to Depends.
    - Replaced ufraw-batch to ufraw. (closes: #755853)
    - Added rawtherapee, imagemagick, brasero and gimp to Suggests.
    Thanks for the bug reports.

  * debian/compat
    - Bump to version 9.

  * debian/patches
    - Deleted desktop_fix.patch, applied by upstrean.
    - Deleted rename_mutex_type.patch, applied by upstream.
    - Created privacy-breach-generic_fix.patch
    - Refresh patches.

  * debian/rules
    - Switched to version's 9 format.

  * debian/copyrigth
    - Updated upstream email.

 -- Santiago Torres Batan <santi73@gmail.com>  Thu, 24 Jul 2014 12:27:54 -0300

fotoxx (14.03.1-1) unstable; urgency=medium

  * New upstream release (closes: #683347)

  * Applied non maintainer changes. Thanks to Jakub Wilk.

  * debian/watch
    - Update download URL

  * debian/patches
    - Moved documentation back to doc/fotoxx and deleted docs_dir.patch.
      Thanks to Maximiliano Curia.
    - Added fotoxx.h and zfuncs.cc files to mutex_rename.patch.
    - Refreshed all patches.
    - Created dektop_fix.patch to fix .desktop using desktop-file-validate.

  * debian/control
    - Bump Standars-Version to 3.9.5.
    - Updated build dependency from libtiff4-dev to libtiff-dev
      (closes: #736003)
    - Added liblcml2-dev to Build-Depends.
    - Created fotoxx-common package for independent arquitecture data.
    - Updated Fotoxx Homepage.

  * debian/copyright
    - Updated copyright years for the upstream.

  * debian/rules
    - Export buildflags.
    - Remove rules that delete some no longer installed files.

 -- Santiago Torres Batan <santi73@gmail.com>  Sat, 15 Mar 2014 12:36:13 -0300

fotoxx (11.11.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Rename mutex type to mutex_tp (closes: #665676). Thanks to Michael Biebl
    for the bug report.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 29 Apr 2012 15:51:13 +0200

fotoxx (11.11.1-1) unstable; urgency=low

  * New upstream release

  * debian/watch
    -Fix lintian warning: debian_watch_contains_dh_make_template

  * debian/rules
    -Remove -XTRANLATIONS -XCHANGES params in dh_compress.
    -Fix lintian warning: debian_rules_missing_recommends_targets
    -Add symlink to avoid duplicate documentation at intalldocs rule.
    -Remove unnecessary files.

  * debian/control
   -Bump Standars-Version to 3.9.2
   -Remov libfreeimage-dev, libimage-exiftool-perf from
     Build-Depends
   -Add xgd-utils and libimage-exiftool-perf to Recommends
   -Add brasero to Suggests

  * debian/copyright
    -Update copyright to 2010, 2011

  * debian/patches/
    -Fix lintian warning format-3.0-but-debian-changes-patch and
     (closes: #643119) by creating:
    -docs_dir.patch: changes upstream sources for the program to
     know where documentation is installed
    -makefile_changes.patch: edit makefile for installation
     Removes the depencies check by dependencies.sh. (closes: #622315)
     Fix FTBFS with binutils-gold. (closes: #610545)
     fotoxxfotoxx.desktop was created by mistake. (closes: #610544)
     Thanks to Mahyuddin Susanto.

  * debian/doc-base
    -Register fotoxx user guide documentation.

 -- Santiago Torres Batan <santi73@gmail.com>  Mon, 07 Nov 2011 16:37:55 -0300

fotoxx (10.7-1) unstable; urgency=low

  * New upstream release
  * debian/control
    -Bump Standars-Version to 3.9.0 No further changes.
    -Changed Reccommends: ufraw-batch instead of ufraw. closes: #583084.

 -- Santiago Torres Batan <santi73@gmail.com>  Thu, 08 Jul 2010 18:24:21 -0300

fotoxx (10.3.1-1) unstable; urgency=low

  * New upstream release
  * Switch to quilt (3.0)
  * debian/control
    -Bump Standars-Version to 3.8.4 No further changes.
    -Added libtiff4-dev to Build-Depends.
  * New versions of fotoxx don't suggest printoxx anymore. closes: #582244

 -- Santiago Torres Batan <santi73@gmail.com>  Sat, 22 May 2010 13:37:32 -0300

fotoxx (9.2-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Wed, 06 Jan 2010 23:03:44 -0300

fotoxx (8.7-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Thu, 26 Nov 2009 18:59:39 -0300

fotoxx (8.6.2-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Thu, 29 Oct 2009 19:34:43 -0300

fotoxx (8.6-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Tue, 13 Oct 2009 23:05:46 -0300

fotoxx (8.5.2-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Sat, 10 Oct 2009 13:06:31 -0300

fotoxx (8.4.3-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Tue, 22 Sep 2009 08:08:16 -0300

fotoxx (8.4.1-1) unstable; urgency=low

  * New upstream release

 -- Santiago Torres Batan <santi73@gmail.com>  Mon, 14 Sep 2009 22:44:23 -0300

fotoxx (8.3-1) unstable; urgency=low

  * Initial release (Closes: #504360)

 -- Santiago Torres Batan <santi73@gmail.com>  Thu, 20 Aug 2009 12:27:47 -0300
