Fotoxx Change Log
=================

Version 20.08 Feb 19 2020
 • Add Portuguese translation

Version 20.07 Feb 17 2020
 • Add libraries to appimage container possibly missing if KDE desktop.

Version 20.06 Feb 10 2020
 • minor change in image zoom from mouse wheel
 • Trim/Rotate: better logic to decide if mouse drag trims or levels image.
 • Raw Therapee: search for the .tif output file in all top image folders.
 • changed contact address to mkornelix@gmail.com

Version 20.05 Jan 29 2020
 • appimage container: add later release of libjpeg.so.8, remove libjasper.
 • bugfix: crash in processing of compressed translation file.
 • bugfix: exit unconditionally if exiftool not installed.

Version 20.04 Jan 08 2020
 • appimage desktop file: enclose Exec= path in quotes to allow embedded blanks.
 • improve popup tip for [+-] button, better clarity for new users.
 • correct user guide zoom button references from 'ZOOM±' to '[+-]'.
 • add 'Thumb View' to gallery menu - set gallery view mode to thumbnails.
 • remember position and size of popup windows for user guide and most reports.

Version 20.03 Jan 06 2020
   When a new release is started for the first time:
      • initialize 'metadata_short_list' and append user additions from old list
      • copy the updated 'userguide' file unconditionally

Version 20.02 Jan 05 2020
 • bugfix: crash if Area Find Gap is restarted (new click) while still running.
 • bugfix: wrong jpeg library in appimage build caused .heic file errors. 
 • disallow adding custom indexed metadata which is indexed by default.
 
Version 20.01 Jan 03 2020
   Remove diagnostic log accidentally left in source code
 
Version 20.0  Jan 01 2020
------------
 • Image Index:
    • First Startup: dialog for initial decision about image indexing.
    • Cancel and restart will resume in place instead of starting over.
    • Track progress for thumbnail updates along with metadata index updates.
    • Files on removable media: index data and thumbnails are no longer lost 
      if Fotoxx is sometimes operated without the removable media mounted.
    • Tools > Quick Index: quick incremental index with no user interaction
      (capture files created/changed by outside agent during Fotoxx session).
 • Denoise: 
    • median method: over 10x faster for large radius and large image.
    • anneal and chroma: new methods, very effective with parameter fiddling.
    • voodoo: new method, no fiddling, fast and usually good enough.
 • RAW file editing, new options: 
    • Use dcraw or RawTherapee, interactive or batch.
    • Expand brightness distribution if an image is abnormally dark.
    • Convert RAW color space using the camera embedded image as a guide.
    • Save-as new version produces a 16-bits/color TIFF file by default.
 • Batch RAW processing additions: 
    • map and compensate dead pixels (constant dark or colored pixels).
    • measure and compensate pixel RGB bias (camera sensor small variations). 
 • File permissions:
    • File > Permissions: view and set permissions for current file.
    • File Save > new version: copy permissions from original/prior version.
    • File Save > new file: dialog shows permissions and allows revision.
 • Edit > Retouch: now includes color balance and saturation adjustments.
 • Edit > Auto Upright: usually automatic, but also has [-90] [+90] buttons.
 • Edit > Trim/Rotate: includes auto-upright and [-90] [+90] buttons.
 • Edit > Trim/Rotate: faster/smoother response to leveling by mouse drag.
 • Enhance > Chromatic: fix lateral and axial chromatic aberration.
 • Edit > Undo Edits: locally/gradually undo prior edit via mouse painting.
 • Combine > Stack/Slider: view two overlaid images with sliding separator.
 • File > GIF Animation: play an animated GIF file in a popup window.
 • Warp > Escher Spiral: Transform image into recursive inward spiral.
 • Edit > Markup: includes draw text, line/arrow, box, oval/circle.
 • Gallery > List View: a dense file name list, similar to Nautilus list view.
 • Gallery > Meta View: list files with thumbnails and key metadata items. 
 • Gallery > Select Files: select files for subsequent batch/script function.
 • Edit > Resize: buttons were added for resize to 1.5x, 2x, and 3x.
 • Tools > Preferences: choose program for playing videos: totem, vlc, others.
 • Zonal Retinex memory reduced by 40% (30 megapixel image needs about 4 GB). 
 • Symbolic links: these can now be used with Fotoxx if you know the rules.
 • Apple .heic files and jpeg2000 .jp2 files can be viewed and edited. 
 • Maximum gallery and album size was increased from 25K to 40K image files.
 • Edit menus were reorganized and some names were shortened.
 • Fotoxx startup: if previous file not found, fall back to previous gallery.
 • Tools > Magnify: UI improvement, more responsive mouse tracking. 
 • Manage Albums: user interface is simpler and faster for common tasks.
 • Search Images: option to select only original + newest version.
 • Shell script: saved Fotoxx Search criteria can be used to select files.
 • Slide Show: display of image file name is optional, time is adjustable.
 • Invalid UTF-8 text in image metadata is detected and suppressed. 
 • Metadata text on image: show any combination of file name/caption/comment.
 • Show RGB: keep pixel positions when a new image is opened (compare images).
 • User Guide was simplified and updated. Web browser is no longer used.
 • Warn immanent "out of memory" failure before vanishing without a trace.
 • Mashup UI: more reliable capture and drag to resize overlay image or text.
 • Reduced slide show jitter by using kernel function sched_setaffinity().
 • Scripting enabled: Resize, Cartoon, Emboss, Pattern, Dither, Brite Ramp.
 • Maps: show markers for images with geocoordinates but no location names.
 • Tools > Missing Translations: export the translation .po file for edit. 
 • Tools > Appimage Files: list files included in the Appimage container.
 • Tools > Preferences: set compression methods/levels for TIFF/PNG files.
 • Select Area by color: smoother mouse tracking and image update.
 

